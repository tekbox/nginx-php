#!/bin/bash

cp -r /app/template/etc/nginx/* /etc/nginx
cp -r /app/template/etc/php5/* /etc/php5

supervisorctl restart nginx
supervisorctl restart php5
