## copy default volumes ##

docker exec -it <container_name> /app/copy-volume.sh

Volumes:

* "./volumes/www/:/usr/share/nginx/html"
* "./volumes/nginx/:/etc/nginx"
*  "./volumes/php5/:/etc/php5"